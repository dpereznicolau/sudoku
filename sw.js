self.addEventListener('install', function(e) {
    e.waitUntil(
      caches.open('sudoku').then(function(cache) {
        return cache.addAll([
          '/sudoku/',
          '/sudoku/index.html',
          '/sudoku/app.js',
          '/sudoku/style.css'
        ]);
      })
    );
   });
   
   self.addEventListener('fetch', function(e) {
     console.log(e.request.url);
     e.respondWith(
       caches.match(e.request).then(function(response) {
         return response || fetch(e.request);
       })
     );
   });