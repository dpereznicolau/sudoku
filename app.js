var sudoku;
var count = 0;
document.getElementById("buttonSolve").addEventListener("click",solve);
document.getElementById("buttonReset").addEventListener("click",reset);


var values = document.getElementsByClassName("number");
for (var i = 0; i < values.length; i++) {
    values[i].addEventListener("keyup",function(e){
        var val = e.target;
        if(val.value != ""){
            if(parseInt(val.value) < parseInt(val.min)){
                val.value = val.min;
            }
            if(parseInt(val.value) > parseInt(val.max)){
                val.value = val.max;
            }
        }
    });
}

function reset() {
    var sudokuTable = document.getElementById("board");
    for (var r = 0; r < sudokuTable.rows.length; r++) {
        for (var c = 0; c < sudokuTable.rows[r].cells.length; c++) {
            sudokuTable.rows[r].cells[c].children[0].value = "";
        }
    }
    resetBackground();
}

function resetBackground() {
    var sudokuTable = document.getElementById("board");
    for (var r = 0; r < sudokuTable.rows.length; r++) {
        for (var c = 0; c < sudokuTable.rows[r].cells.length; c++) {
            sudokuTable.rows[r].cells[c].children[0].style.backgroundColor = "";
        }
    }
}

function solve() {
    sudoku = board_to_array();
    if (testInitialNumbers(sudoku)) {
        if (solveSudoku(sudoku, sudoku.length)) {
            resetBackground();
            array_to_board();
            alert("SOLVED!! Num. of iterations to solve: " + count);
        } else {
            alert("Impossible to solve");
        }
    } else {
        alert("ERROR!! Initial numbers have errors");
    }
    count = 0;
}

function board_to_array() {
    var sudokuTable = document.getElementById("board").rows;
    myArray = []
    for (var i = 0; i < sudokuTable.length; i++) {
            var cols = sudokuTable[i].children
            myRow = []
            for (var j = 0; j < cols.length; j++) {
                    var val = cols[j].children[0].value;
                    if (val == "") val = 0;
                    myRow.push(parseInt(val));
            }
            myArray.push(myRow);
    }
    return myArray;
}

function array_to_board() {
    var sudokuTable = document.getElementById("board");
    for (var r = 0; r < sudokuTable.rows.length; r++) {
        for (var c = 0; c < sudokuTable.rows[r].cells.length; c++) {
            sudokuTable.rows[r].cells[c].children[0].value = sudoku[r][c];
        }
    }
}

function isSafe(board, row, col, num) {
    var sudokuTable = document.getElementById("board");
    //mirar que no existeixi el numero a la fila
    for (var c = 0; c < board.length; c++) {
        if (board[row][c] == num && c != col) {
            sudokuTable.rows[row].cells[c].children[0].style.backgroundColor = "yellow";
            sudokuTable.rows[row].cells[col].children[0].style.backgroundColor = "yellow";
            return false;
        }
    }
    //mirar que no existeixi el numero a la columna
    for (var r = 0; r < board.length; r++) {
        if (board[r][col] == num && r != row) {
            sudokuTable.rows[r].cells[col].children[0].style.backgroundColor = "yellow";
            sudokuTable.rows[row].cells[col].children[0].style.backgroundColor = "yellow";
            return false;
        }
    }
    //mirar que no existeixi el numero al quadrant
    var sqrt = Math.sqrt(board.length);
    var qRowStart = row - row % sqrt;
    var qColStart = col - col % sqrt;

    for (var r = qRowStart; r < qRowStart + sqrt; r++) {
        for (var c = qColStart; c < qColStart + sqrt; c++) {
            if (board[r][c] == num && r != row && c != col) {
                sudokuTable.rows[r].cells[c].children[0].style.backgroundColor = "yellow";
                sudokuTable.rows[row].cells[col].children[0].style.backgroundColor = "yellow";
                return false;
            }
        }
    }
    //si no he trobat coincidencies es numero segur
    return true;
}

function testInitialNumbers(board) {
    for (var r = 0; r < board.length; r++) {
        for (var c = 0; c < board[r].length; c++) {
            var num = board[r][c];
            if (num != 0) {
                if (!isSafe(board, r, c, num)) {
                    return false;
                }
            }
        }
    }
    return true;
}

function solveSudoku(board, n) {
    var row = -1;
    var col = -1;
    var isSolved = true;
    count++;
    for (var i = 0; i < n; i++) {
        for (var j = 0; j < n; j++) {
            if (board[i][j] == 0) {
                row = i;
                col = j;
                isSolved = false;
                break;
            }
        }
        if (!isSolved) {
            break;
        }
    }

    if (isSolved) {
        return true;
    }

    for (var num = 1; num <= n; num++) {
        if (isSafe(board, row, col, num)) {
            board[row][col] = num;
            if (solveSudoku(board, n)) {
                return true;
            } else {
                board[row][col] = 0;
            }
        }
    }
    return false;
}

if('serviceWorker' in navigator) {
    navigator.serviceWorker
             .register('/sudoku/sw.js')
             .then(function() { console.log('Service Worker Registered'); });
}

let deferredPrompt;
const addBtn = document.querySelector('.add-button');
addBtn.style.display = 'none';

window.addEventListener('beforeinstallprompt', (e) => {
  // Prevent Chrome 67 and earlier from automatically showing the prompt
  e.preventDefault();
  // Stash the event so it can be triggered later.
  deferredPrompt = e;
  // Update UI to notify the user they can add to home screen
  addBtn.style.display = 'inline-block';

  addBtn.addEventListener('click', (e) => {
    // hide our user interface that shows our A2HS button
    addBtn.style.display = 'none';
    // Show the prompt
    deferredPrompt.prompt();
    // Wait for the user to respond to the prompt
    deferredPrompt.userChoice.then((choiceResult) => {
        if (choiceResult.outcome === 'accepted') {
          console.log('User accepted the Sudoku prompt');
        } else {
          console.log('User dismissed the Sudoku prompt');
        }
        deferredPrompt = null;
      });
  });
});

